
jQuery(function($) {
    var asr_download = null;
    $('#app_name').change(function() {
        asr_slug();
    });

    $('#add-app-btn').click(function() {
        $('#app-errors').hide();
        var app_id = $('#app_id').val();
        var app_name = $('#app_name').val();
        var app_slug = $('#app_slug').val();
        var regex = /[0-9]+/;
        var errors = [];
        if (!regex.test(app_id)) {
            errors.push('Invalid App ID.');
        }
        if (app_name.length < 1) {
            errors.push('Nickname cannot be blank.');
        }
        if (app_slug.length < 1) {
            errors.push('Slug cannot be blank.');
        }
        if (errors.length > 0) {
            $('#app-errors').html("<b>Errors</b>: <br/>" + errors.join('<br/>')).show();
            return false;
        }
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            dataType: 'JSON',
            data: {
                action: 'asr_add_app',
                app_id: app_id,
                app_name: app_name,
                app_slug: app_slug
            },
            success: function (result) {
                if (result.status == 1) {
                    $('#app_id').val('');
                    $('#app_name').val('');
                    $('#app_slug').val('');
                    var html = '<tr id="app-' + app_id + '">\
                                    <td>' + app_id + '</td>\
                                    <td>' + result.name + '</td>\
                                    <td>' + result.slug + '</td>\
                                    <td style="width: 80px;" class="review-count">0</td>\
                                    <td style="width: 80px;">\
                                        <a href="javascript:void(0)" data-app="' + app_id + '" class="delete-reviews" title="Delete app"><i class="fa fa-close"></i></a>\
                                        &nbsp;&nbsp;&nbsp;\
                                        <a href="javascript:void(0)" data-app="' + app_id + '" class="refresh-reviews" title="Download Reviews"><i class="fa fa-refresh"></i></a>\
                                        &nbsp;&nbsp;&nbsp;\
                                        <a href="javascript:void(0)" data-app="' + app_id + '" class="stop-download" title="Cancel Download"><i class="fa fa-stop"></i></a>\
                                    </td>\
                                    <td>\
                                        [asreviews app="' + result.name + '"]\
                                    </td>\
                                </tr>';
                    $('#asr-apps-list').append(html);
                    $('#app-' + app_id + ' .refresh-reviews').click(function() {
                        asr_refresh_reviews(this);
                    });
                    $('#app-' + app_id + ' .stop-download').click(function() {
                        asr_stop_download(this);
                    });
                    $('#app-' + app_id + ' .delete-reviews').click(function() {
                        asr_delete_app(this);
                    });
                    asr_refresh_reviews($('#app-' + app_id + ' .refresh-reviews'));
                } else {
                    $('#app-errors').html("<b>Errors</b>: <br/>" + result.error).show();
                }
            },
            error: function (result) {
                console.error(result);
                errors.push = "Communication error";
                $('#app-errors').html("<b>Errors</b>: <br/>" + errors.join('<br/>')).show();
            }
        });
    });

    $('.refresh-reviews').click(function() {
        asr_refresh_reviews(this);
    });

    $('.stop-download').click(function() {
        asr_stop_download(this);
    });

    $('.delete-reviews').click(function() {
        asr_delete_app(this);
    });

    function asr_delete_app(el) {
        var $this = $(el);
        var app_id = $this.attr('data-app');
        $this.find('i').addClass('fa-spin');
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
                action: 'asr_delete_app',
                app_id: app_id
            },
            success: function (result) {
                $('#app-' + app_id).remove();
            },
            error: function (result) {
                console.error(result);
                alert('Communication error');
            },
            always: function () {
                $this.find('i').removeClass('fa-spin');
            }
        });
    }

    function asr_stop_download(el) {
        var $this = $(el);
        if (asr_download) {
            asr_download.abort();
            asr_download = null;
        }
        $this.parent().find('a.refresh-reviews i').removeClass('fa-spin');
        $this.hide();
    }

    function asr_refresh_reviews(el) {
        var $this = $(el);
        if (asr_download) {
            alert('Another download is running.  Please click the refresh button for this app once the current download completes.');
        } else {
            $this.find('i').addClass('fa-spin');
            $this.parent().find('.stop-download').show();
            $('.reviews-status').show();
            var $log = $('.reviews-status-log');
            $log.empty();
            var app_id = $this.attr('data-app');
            $log.append('Initating download for App ID: ' + app_id + '<br/>');
            asr_ajax(app_id, 0);
        }
    }

    function asr_ajax(app_id, cc_index) {
        var $log = $('.reviews-status-log');
        if (asr_countries.length > cc_index) {
            var cc = asr_countries[cc_index];
            asr_download = $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    action: 'asr_download_reviews',
                    app_id: app_id,
                    cc: cc,
                    first: (cc_index == 0 ? 1 : 0)
                },
                success: function (result) {
                    $log.append(result.msg + "<br/>");
                    $('#app-' + app_id + ' td.review-count').html(result.total);
                    cc_index++;
                    if (asr_countries.length <= cc_index) {
                        $log.append('Total Reviews: ' + result.total + "<br/>");
                    }
                    $log.scrollTop($log[0].scrollHeight);
                    asr_ajax(app_id, cc_index);
                },
                error: function (result) {
                    if (result.statusText == 'abort') {
                        $log.append("Aborted!");
                        $log.scrollTop($log[0].scrollHeight);
                    } else {
                        console.log(result);
                        $log.append(cc + ": communication error<br/>");
                        $log.scrollTop($log[0].scrollHeight);
                        cc_index++;
                        asr_ajax(app_id, cc_index);
                    }
                }
            });
        } else {
            asr_download = null;
            $('.reviews-apps td a[data-app=' + app_id + '] i').removeClass('fa-spin');
            $('.reviews-apps tr#app-' + app_id + ' a.stop-download').hide();
            $log.append('DONE!');
            $log.scrollTop($log[0].scrollHeight);
        }
    }

    function asr_slug() {
        var $slug = $('#app_slug');
        if ($slug.val() == '') {
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                async: false,
                data: {
                    action: 'asr_app_slug',
                    name: $('#app_name').val()
                },
                success: function (slug) {
                    console.log(slug);
                    $slug.val(slug);
                }
            });
        }
    }
});