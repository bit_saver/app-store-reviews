<?php

class AS_Entry {

    public $id;
    public $title;
    public $rating;
    public $content;
    public $author;
    public $link;
    public $perma;
    public $days;
    public $country;
    public $cc;
    public $date;

    function __construct($entry) {
        $this->id = $entry->id;
        $this->title = $entry->title;
        $this->rating = 0;
        $this->rating = $entry->rating;
        if ($entry->content) {
            $this->content = $entry->content;
        }
        $this->author = $entry->author;
        $this->link = $entry->link;
        $this->perma = $entry->perma;
        $this->country = $entry->country;
        $this->cc = $entry->cc;
        $this->days = $entry->days;
        $this->date = $entry->date;
    }

    /**
     * @param SimpleXMLElement $entry
     * @return array
     */
    static function xml($entry) {
        $ret = array();
        $ret['id'] = (string) $entry->id;
        $ret['title'] =(string)  $entry->title;
        $ret['rating'] = 0;
        if ($entry->rating) {
            $ret['rating'] = (string) $entry->rating;
        }
        if ($entry->content) {
            $ret['content'] = nl2br((string) $entry->content);
//            foreach ($entry->content as $content) {
//                $ret['content'] .= (string) $content;
//            }
        }
        $ret['author'] = (string) $entry->author->name;
        $ret['link'] = (string) $entry->author->uri;
        $d = (string) $entry->updated;
        $ret['date'] = date('F j, Y', strtotime($d));
        $date = new DateTime($d);
        $now = new DateTime();
        $diff = $now->diff($date);
        $ret['days'] = $diff->days;
        $ret['country'] = (string) $entry->country;
        $ret['cc'] = (string) $entry->cc;
        $ret['perma'] = (string) $entry->perma;
        return $ret;
    }

}