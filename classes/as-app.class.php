<?php

class AS_App {

    public $name;
    public $author;
    public $link;
    public $image;

    function __construct($entry) {
        $this->name = $entry->name;
        $this->author = $entry->author;
        $this->link = $entry->link;
        $this->image = $entry->image;
    }

    /**
     * @param SimpleXMLElement $entry
     * @return array
     */
    static function xml($entry) {
        return array(
            'name' => (string) $entry->name,
            'author' => (string) $entry->artist,
            'image' => (string) end($entry->image),
        );
    }

    function toArray() {
        return array(
            'name' => $this->name,
            'author' => $this->author,
            'image' => $this->image
        );
    }

}