<div class="asr-review" id="review-<?=$entry->id?>">
    <div class="asr-review-header">
        <div class="asr-review-title">
            <a href="<?=site_url() . '/' . $entry->perma?>" target="_blank">
                <h1>
                    <?=$entry->title?>
                </h1>
            </a>
        </div>
        <div class="asr-review-rating">
            <?php for ($i = 0; $i < $entry->rating; $i++): ?>
                <i class="fa fa-star">&nbsp;</i>
            <?php endfor; ?>
        </div>
    </div>
    <?php if ($entry->content): ?>
        <div class="asr-review-content"><?=$entry->content?></div>
    <?php endif; ?>
    <div class="asr-review-author">
        by <?=$entry->author?> &nbsp;&bull;&nbsp; <span class="flag-icon flag-icon-<?=$entry->cc?>">&nbsp;</span>&nbsp;<?=$entry->country?> &nbsp;&bull;&nbsp; <?=($entry->days > 30 ? $entry->date : "$entry->days days ago")?>
    </div>
    <?php if ($isAdmin): ?>
        <div class="blacklist">
            <a href="javascript:void(0)" data-review="<?=$entry->id?>" data-author="<?=$entry->author?>" class="blacklist-review" onclick="asr_blacklist(this); return false;"><i class="fa fa-trash"></i></a>
        </div>
    <?php endif; ?>
    <?php if ($review_id) {
        include $dir . '/templates/app-info.php';
    }?>

</div>