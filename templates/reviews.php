<div class="asr-reviews">
    <?php
//    include $dir . '/templates/app-header.php';
    foreach ($reviews as $entry) {
        $entry = new AS_Entry($entry);
        if (!in_array($entry->id, $blacklist->reviews)) {
            if (!$review_id || $review_id == $entry->id) {
                if ($entry->rating && $entry->rating > 3) {
                    include $dir . '/templates/single-review.php';
                }
            }
        }
    }
    ?>
</div>
