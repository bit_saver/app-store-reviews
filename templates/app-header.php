<div class="asr-app">
    <div class="app-images">
        <img src="<?=$app->image?>"/>
    </div>
    <div class="app-info">
        <h1 class="app-details"><?=$app->name?></h1>
        <h2 class="app-details"><?=$app->author?></h2>
    </div>
</div>