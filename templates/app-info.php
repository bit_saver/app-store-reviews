<div class="asr-app-info">
    <div class="app-images">
        <img src="<?=$app->image?>"/>
    </div>
    <div class="app-info">
        <div class="app-details app-details-name"><?=$app->name?></div>
        <div class="app-details app-details-author"><?=$app->author?></div>
    </div>
</div>