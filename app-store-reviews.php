<?php
/*
Plugin Name: App Store Reviews
Plugin URI:
Description: Provides a shortcode for displaying app store reviews for any given App ID
Version: 1.8
Author: Tyler Hebenstreit
Author URI: http://tylerhebenstreit.com
License: GPL2
*/

$asr_reviews = new AS_Reviews();
register_activation_hook(__FILE__, array('AS_Reviews', 'activation'));
register_deactivation_hook(__FILE__, array('AS_Reviews', 'deactivation'));

class AS_Reviews {

    public function __construct() {
        require_once dirname(__FILE__) . '/classes/as-entry.class.php';
        require_once dirname(__FILE__) . '/classes/as-app.class.php';
        if (is_admin()) {
            add_action('admin_menu', array($this, 'asr_admin'));
            add_action('admin_init', array($this, 'register_settings'));
            $plugin = plugin_basename(__FILE__);
            add_filter("plugin_action_links_$plugin", array($this, 'asr_settings_link'));

            add_action('wp_ajax_asr_download_reviews', array($this, 'ajax_download_country'));
            add_action('wp_ajax_asr_delete_app', array($this, 'ajax_delete_app'));
            add_action('wp_ajax_asr_add_app', array($this, 'ajax_add_app'));
            add_action('wp_ajax_asr_app_slug', array($this, 'ajax_app_slug'));
        } else {
            add_action('init', array('AS_Reviews', 'rewrite_rules'));
            add_shortcode('asreviews', array($this, 'shortcode'));
            wp_enqueue_style('app-store-reviews', plugins_url('app-store-reviews/css/style.css'));
        }
        wp_enqueue_style('font-awesome', plugins_url('app-store-reviews/css/font-awesome.min.css'));
        wp_enqueue_style('flag-icons', plugins_url('app-store-reviews/css/flag-icon.min.css'));
	    add_action('wp_ajax_nopriv_asr_blacklist_review', array($this, 'ajax_blacklist_review'));
        add_action('wp_ajax_asr_blacklist_review', array($this, 'ajax_blacklist_review'));
    }

    static function activation() {
        AS_Reviews::rewrite_rules();
        flush_rewrite_rules();
    }

    static function rewrite_rules() {
        add_rewrite_rule('^app-store-review/([^/]*)/([^/]*)/?', 'index.php?pagename=app-store-review&app=$matches[1]&review=$matches[2]', 'top');
        add_rewrite_tag('%app%','([^/]*)');
        add_rewrite_tag('%review%','([^/]*)');
    }

    static function deactivation() {
        flush_rewrite_rules();
    }

    function register_settings() {
        register_setting('asr_reviews', 'asr_reviews_apps');
    }

    function asr_settings_link($links) {
        $settings_link = '<a href="options-general.php?page=asreviews">Settings</a>';
        array_unshift($links, $settings_link);
        return $links;
    }

    function asr_admin() {
        add_options_page( 'App Store Reviews', 'App Store Reviews', 'manage_options', 'asreviews', array($this, 'asr_admin_page') );
    }

    function asr_admin_page() {
        $dir = dirname(__FILE__);
        $apps = get_option('asr_reviews_apps', array());
        foreach ($apps as $app_id => $app) {
            $json = $dir . "/data/$app_id.json";
            $count = 0;
            if (file_exists($json)) {
                $data = json_decode(file_get_contents($json));
                $count = count(array_keys((array)$data->reviews));
            }
            $apps[$app_id]['reviews'] = $count;
        }
        $countries = array();
        $cc_file = dirname(__FILE__) . "/data/countries.csv";
        if (file_exists($cc_file)) {
            $fp = fopen($cc_file, 'r');
            while ($fp && ($line = fgetcsv($fp))) {
                $countries[] = $line[0];
            }
            fclose($fp);
        }
        ?>
        <style type="text/css">
            .reviews-apps {
                width: 600px;
                float: left;
                margin-right: 10px;
            }
            .asr-apps {
                background: #fff;
                width: 100%;
            }
            .asr-apps th {
                text-align: left;
            }
            .asr-apps tbody tr:nth-of-type(2n-1) {
                background: rgb(246, 246, 246);
            }
            .asr-apps tbody tr:nth-of-type(2n) {
                background: rgb(252, 252, 252);
            }
            .asr-apps tbody td {
                padding: 3px;
            }

            .reviews-status {
                width: calc(100% - 620px);
                float: left;
                display: none;
                min-width: 600px;
            }
            .reviews-status-log {
                border: 1px solid #000;
                background: #fff;
                width: 100%;
                height: 400px;
                overflow-y: scroll;
            }

            .delete-reviews {
                color: red;
            }
            .stop-download {
                color: #f3e016;
                display: none;
            }

            .app-form {
                max-width: 700px;
                clear: both;
                margin: 10px 0;
                display: inline-block;
            }
            .app-field {
                width: auto;
                float: left;
                margin-right: 42px;
            }
            .app-field:last-of-type {
                margin-right: 0;
            }
            .app-add, #app-errors {
                clear: both;
                float: left;
                padding-top: 10px;
            }
            #app-errors {
                background-color: #f2dede;
                border-color: #ebcccc;
                color: #a94442;
                margin-top: 10px;
                padding: 10px;
                display: none;
            }
        </style>
        <div class="wrap">
            <h1>App Store Reviews</h1>
            <hr>
            <div><strong>Note:</strong> For individual reviews to work, a page with slug <code>app-store-review</code> must exist and contain the shortcode <code>[asreviews]</code></div>
            <hr>
            <h2>New App</h2>
            <div class="app-form">
                <div class="app-field">
                    <div class="app-label">
                        <label for="app_id">App ID</label>
                    </div>
                    <div class="app-input">
                        <input type="text" name="app_id" id="app_id"/>
                    </div>
                </div>
                <div class="app-field">
                    <div class="app-label">
                        <label for="app_name">Nickname</label>
                    </div>
                    <div class="app-input">
                        <input type="text" name="app_name" id="app_name"/>
                    </div>
                </div>
                <div class="app-field">
                    <div class="app-label">
                        <label for="app_slug">Slug</label>
                    </div>
                    <div class="app-input">
                        <input type="text" name="app_slug" id="app_slug"/>
                    </div>
                </div>
                <div id="app-errors">

                </div>
                <div class="app-add">
                    <button type="button" class="button button-primary" id="add-app-btn">Add and Download</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="wrap">
            <div class="reviews-apps">
                <h3>Existing Apps</h3>
                <table class="asr-apps">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nickname</th>
                        <th>Slug</th>
                        <th style="width: 80px;">Reviews</th>
                        <th style="width: 80px;">Actions</th>
                        <th style="width: 180px">Shortcode</th>
                    </tr>
                    </thead>
                    <tbody id="asr-apps-list">
                    <?php foreach ($apps as $app_id => $app):?>
                        <tr id="app-<?=$app_id?>">
                            <td><?=$app_id?></td>
                            <td><?=$app['name']?></td>
                            <td><?=$app['slug']?></td>
                            <td class="review-count"><?=$app['reviews']?></td>
                            <td>
                                <a href="javascript:void(0)" data-app="<?=$app_id?>" class="delete-reviews" title="Delete App"><i class="fa fa-close"></i></a>
                                &nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0)" data-app="<?=$app_id?>" class="refresh-reviews" title="Download Reviews"><i class="fa fa-refresh"></i></a>
                                &nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0)" data-app="<?=$app_id?>" class="stop-download" title="Stop Download"><i class="fa fa-stop"></i></a>
                            </td>
                            <td>
                                [asreviews app="<?=$app['name']?>"]
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="reviews-status">
                <h3>Download Status</h3>
                <div class="reviews-status-log">

                </div>
            </div>
        </div>
        <script type="text/javascript">
            var asr_countries = <?=json_encode($countries)?>;
        </script>
        <script src="<?=plugins_url("app-store-reviews/js/admin.js")?>" type="text/javascript"></script>
        <?php
    }

    public function shortcode($atts) {
        $isAdmin = current_user_can('manage_options');
        $a = shortcode_atts(array('appid' => 0, 'app' => null), $atts);
        $app_id = $a['appid'];
        $app_name = $a['app'];
        $app = null;
        if (get_query_var('app')) {
            $slug = get_query_var('app');
            $id = $this->getAppBySlug($slug);
            if ($id) {
                $app_id = $id;
            }
        } else if ($app_name) {
            $id = $this->getAppByName($app_name);
            if ($id) {
                $app_id = $id;
            }
        }
        $review_id = null;
        if (get_query_var('review')) {
            $review_id = get_query_var('review');
        }
        if (!$app_id) {
            return "No App ID.";
        }
        $dir = dirname(__FILE__);
        $json = $dir . "/data/$app_id.json";
        if (!file_exists($json)) {
            return "JSON not found.";
        }
        $reviews = json_decode(file_get_contents($json));
        $app = new AS_App($reviews->app);
        $reviews = (array) $reviews->reviews;
        $blacklist_file = "$dir/data/blacklist.json";
        $blacklist = (object) array();
        $blacklist->reviews = array();
        if (file_exists($blacklist_file)) {
            $blacklist = json_decode(file_get_contents($blacklist_file));
        }
        ob_start();
        include $dir . '/templates/reviews.php';
        ?>
        <?php if ($isAdmin): ?>
            <script type="text/javascript">
	            if (typeof asr_blacklist != 'function') {
		            function asr_blacklist(el) {
			            var $ = jQuery.noConflict();
			            var $this = $(el);
			            var review_id = $this.attr('data-review');
			            $.ajax({
				            url: '<?=admin_url('admin-ajax.php')?>',
				            type: 'POST',
				            data: {
					            action: 'asr_blacklist_review',
					            review_id: review_id
				            },
				            success: function (result) {
					            console.log(result);
					            alert('Review blacklisted.');
					            $('#review-' + review_id).remove();
				            },
				            error: function (result) {
					            console.error(result);
					            alert('Communication error');
				            }
			            });
		            }
	            }
            </script>
        <?php endif; ?>
        <?php
        return ob_get_clean();
    }

    function getAppBySlug($slug) {
        $apps = get_option('asr_reviews_apps', array());
        foreach ($apps as $app_id => $app) {
            if ($app['slug'] == $slug) {
                return $app_id;
            }
        }
        return null;
    }

    function getAppByName($name) {
        $apps = get_option('asr_reviews_apps', array());
        foreach ($apps as $app_id => $app) {
            if ($app['name'] == $name) {
                return $app_id;
            }
        }
        return null;
    }

    function getSlugByAppID($id) {
        $apps = get_option('asr_reviews_apps', array());
        if (array_key_exists($id, $apps)) {
            return $apps[$id]['slug'];
        }
        return null;
    }

    function ajax_add_app() {
        $ret = array('status' => 1);
        $app_id = $_POST['app_id'];
        $app_name = $_POST['app_name'];
        $app_slug = $_POST['app_slug'];
        $app_slug = sanitize_title_with_dashes($app_slug);

        $apps = get_option('asr_reviews_apps', array());

        if (array_key_exists($app_id, $apps)) {
            $ret['status'] = 0;
            $ret['error'] = 'App ID already exists.';
            echo json_encode($ret);
            exit;
        }
        foreach ($apps as $id => $app) {
            if ($app['name'] == $app_name) {
                $ret['status'] = 0;
                $ret['error'] = 'App already exists with name: ' . $app_name;
                echo json_encode($ret);
                exit;
            }
            if ($app['slug'] == $app_slug) {
                $ret['status'] = 0;
                $ret['error'] = 'App already exists with slug: ' . $app_slug;
                echo json_encode($ret);
                exit;
            }
        }
        $apps[$app_id] = array('id' => $app_id, 'name' => $app_name, 'slug' => $app_slug);
        $ret['name'] = $app_name;
        $ret['slug'] = $app_slug;
        update_option('asr_reviews_apps', $apps);
        echo json_encode($ret);
        exit;
    }

    function ajax_download_country() {
        set_time_limit(120);
        $app_id = $_POST['app_id'];
        $cc = $_POST['cc'];
        $first = $_POST['first'];
        $slug = $this->getSlugByAppID($app_id);
        $reviews = array(
            'app' => null,
            'reviews' => array()
        );
        $json = dirname(__FILE__) . "/data/$app_id.json";
        if (!file_exists($json)) {
            file_put_contents($json, json_encode($reviews));
        } else {
            $reviews_orig = json_decode(file_get_contents($json));
            $reviews['app'] = $reviews_orig->app;
            foreach ($reviews_orig->reviews as $key => $val) {
                $reviews['reviews'][$key] = $val;
            }
        }

        $countries = array();
        $fp = fopen(dirname(__FILE__) . '/data/countries.csv', 'r');
        while ($fp && ($line = fgetcsv($fp))) {
            $countries[$line[0]] = $line[1];
        }
        fclose($fp);
        $country = $countries[$cc];

        if ($first) {
            $this->log("Getting reviews for: $slug");
        }

        $url = "https://itunes.apple.com/rss/customerreviews/id=$app_id/xml?sortBy=mostRecent&cc=$cc";
        $results = file_get_contents($url);
        if ($results) {
            $results = str_replace('im:', '', $results);
            $xml = simplexml_load_string($results);
            if ($xml) {
                $entries = $xml->entry;
                if (count($entries)) {
                    if (!$reviews['app']) {
                        $reviews['app'] = AS_App::xml($entries[0]);
                    }
                    unset($entries[0]);
                    $count = 0;
                    $duplicates = 0;
                    foreach ($entries as $entry) {
                        $id = (int) $entry->id;
                        if ($reviews['reviews'][$id]) {
                            $duplicates++;
                        } else {
                            $entry->country = $country;
                            $entry->cc = $cc;
                            $entry->perma = 'app-store-review/' . $slug . '/' . $id;
                            $entry = AS_Entry::xml($entry);
                            $reviews['reviews'][$id] = $entry;
                            $count++;
                        }
                    }
                    $str = "$country: $count reviews added ($duplicates duplicates ignored)";
                } else {
                    $str = "$country: NO reviews";
                    $this->log($str);
                }
            } else {
                $str = "$country: NO reviews";
                $this->log($str);
            }
        } else {
            $str = "$country: NO reviews";
            $this->log($str);
        }
        uasort($reviews['reviews'], 'AS_Reviews::cmp_days');
        file_put_contents($json, json_encode($reviews));
        echo json_encode(array('msg' => $str, 'total' => count($reviews['reviews'])));
        exit;
    }

    function ajax_delete_app() {
        $app_id = $_POST['app_id'];
        $apps = get_option('asr_reviews_apps');
        unset($apps[$app_id]);
        update_option('asr_reviews_apps', $apps);
        $dir = dirname(__FILE__);
        $json = "$dir/data/$app_id.json";
        if (file_exists($json)) {
            unlink($json);
        }
        echo 1;
        return;
    }

    function ajax_blacklist_review() {
        $this->log('ajax_blacklist_review');
        $dir = dirname(__FILE__);
        $file = "$dir/data/blacklist.json";
        $blacklist = (object) array();
        $blacklist->reviews = array();
        if (!file_exists($file)) {
            file_put_contents($file, '');
        } else {
            $blacklist = json_decode(file_get_contents($file));
        }
        $review_id = $_POST['review_id'];
        if (!in_array($review_id, $blacklist->reviews)) {
            $blacklist->reviews[] = $review_id;
        }
        $this->log('updated blacklist');
        $this->log($blacklist);
        file_put_contents($file, json_encode($blacklist));
        echo 1;
        die();
    }

    function ajax_app_slug() {
        $name = $_POST['name'];
        echo sanitize_title_with_dashes($name);
        exit;
    }

    static function cmp_days($a, $b) {
        if ($a->days > $b->days)
            return 1;
        else if ($a->days < $b->days)
            return -1;
        else
            return 0;
    }

    private function log($str) {
        file_put_contents(plugin_dir_path(__FILE__) . 'asr.log', "[" . date('m/d/y H:i:s') . "] " . print_r($str,1) . "\r\n", FILE_APPEND);
    }

}
